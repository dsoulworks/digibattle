﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NameGenerator : MonoBehaviour
{
    public Text nameText;

    void Start()
    {
        GenerateName();
    }

    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GenerateName();
        }
    }

    private void GenerateName ()
    {
        string[] allLetter = new string[] {"a","b","c","d","e","f","g","h","i","j","k","l","m",
                                           "n","o","p","q","r","s","t","u","v","w","x","y","z"};

        string[] vocalLetter = new string[] { "a", "i", "u", "e", "o" };

        string[] consonantLetter = new string[] {"b","c","d","f","g","h","j","k","l","m",
                                                 "n","p","q","r","s","t","v","w","x","y","z"};

        string theName = "";

        int totalLetter = 0;
        totalLetter = Random.Range(3, 9);

        string[] nameLetter = new string[totalLetter];

        for(int i = 0; i < totalLetter; i++)
        {
            string pickLetter = allLetter[Random.Range(0, allLetter.Length)];
            
            if (i > 1)
            {
                int vocalCount = 0;
                int consoCount = 0;

                for (int v = 0; v < vocalLetter.Length; v++)
                {
                   if (nameLetter[i-2] == vocalLetter[v])
                    {
                        vocalCount++;
                    }

                    if (nameLetter[i - 1] == vocalLetter[v])
                    {
                        vocalCount++;
                    }
                }

                if (vocalCount > 1)
                {
                    pickLetter = consonantLetter[Random.Range(0, consonantLetter.Length)];
                }
                else
                {
                    for (int c = 0; c < consonantLetter.Length; c++)
                    {
                        if (nameLetter[i - 2] == consonantLetter[c])
                        {
                            consoCount++;
                        }

                        if (nameLetter[i - 1] == consonantLetter[c])
                        {
                            consoCount++;
                        }
                    }

                    if (consoCount > 1)
                    {
                        pickLetter = vocalLetter[Random.Range(0, vocalLetter.Length)];
                    }
                }    
            }

            nameLetter[i] = pickLetter;
        }

        for(int n = 0; n < totalLetter; n++)
        {
            theName = theName + nameLetter[n];
        }

        nameText.text = theName.ToUpper();
    }

}
